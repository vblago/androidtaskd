package ltd.vblago.taskd.model;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;

public class MyStopwatch {

    private MyCallback callback;
    private float[] time;
    private static final int START = 1;
    public boolean isStarted;

    public MyStopwatch() {
        this.time = new float[]{0, 0, 0};
    }

    @SuppressLint("HandlerLeak")
    Handler h = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            if (msg.what == START) {

                time[0] += 0.1;
                callback.showProgress(time);
                if (time[0] >= 60) {
                    time[0] -= 60;
                    time[1]++;
                }
                if (time[1] >= 60) {
                    time[1] -= 60;
                    time[2]++;
                }
                h.sendEmptyMessageDelayed(START, 100);
            }
        }
    };

    public void setCallback(MyCallback callback) {
        this.callback = callback;
    }

    public void startStopwatch() {
        isStarted = true;
        h.sendEmptyMessage(START);
    }

    public void stopStopwatch() {
        if (isStarted) {
            stopStopwatch();
        }
        isStarted = false;
        h.removeMessages(START);
    }

    public void zeroingOut() {
        time = new float[]{0, 0, 0};
        callback.showProgress(time);
    }

    public interface MyCallback {
        void showProgress(float[] time);
    }
}

