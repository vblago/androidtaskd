package ltd.vblago.taskd.model;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;

public class MyTabataTimer {

    private MyCallback callback;
    private float[] time;
    private static final int START = 1;
    public boolean isStarted;
    private static final float work = 20;
    private static final float relax = 5;
    private static final float iter = 3;

    public MyTabataTimer() {
        this.time = new float[]{work, relax, iter};
    }

    @SuppressLint("HandlerLeak")
    private Handler h = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            if (msg.what == START) {
                if (time[0] >= 0.1) {
                    if (time[0] == work) {
                        callback.changeBackgroundColor("work");
                    }
                    time[0] -= 0.1;
                    callback.showProgress(time[0]);
                    h.sendEmptyMessageDelayed(START, 100);
                } else if (time[1] >= 0.1) {
                    if (time[1] == relax) {
                        callback.changeBackgroundColor("relax");
                    }
                    time[1] -= 0.1;
                    callback.showProgress(time[1]);
                    h.sendEmptyMessageDelayed(START, 100);
                } else if (time[2] > 1) {
                    time[0] = work;
                    time[1] = relax;
                    time[2]--;
                    h.sendEmptyMessageDelayed(START, 100);
                } else {
                    callback.changeBackgroundColor("");
                }
            }
        }
    };

    public void startTimer() {
        isStarted = true;
        h.sendEmptyMessage(START);
    }

    public void stopTimer() {
        isStarted = false;
        h.removeMessages(START);
    }

    public void zeroingOut() {
        if (isStarted) {
            stopTimer();
        }
        time = new float[]{work, relax, iter};
        callback.showProgress(time[0]);
    }

    public void setCallback(MyCallback callback) {
        this.callback = callback;
    }

    public interface MyCallback {
        void showProgress(float time);

        void changeBackgroundColor(String color);
    }
}

