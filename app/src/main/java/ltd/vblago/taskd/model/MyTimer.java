package ltd.vblago.taskd.model;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;

public class MyTimer {

    private MyCallback callback;
    private float time;
    private static final int START = 1;
    public boolean isStarted;
    private static final float sec = 30;

    public MyTimer() {
        this.time = sec;
    }

    @SuppressLint("HandlerLeak")
    private Handler h = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            if (msg.what == START) {
                time -= 0.1;
                callback.showProgress(time);
                if (time >= 0.1) {
                    h.sendEmptyMessageDelayed(START, 100);
                }

            }
        }
    };

    public void startTimer() {
        isStarted = true;
        h.sendEmptyMessage(START);
    }

    public void stopTimer() {
        isStarted = false;
        h.removeMessages(START);
    }

    public void zeroingOut() {
        if (isStarted) {
            stopTimer();
        }
        time = sec;
        callback.showProgress(time);
    }

    public void setCallback(MyCallback callback) {
        this.callback = callback;
    }

    public interface MyCallback {
        void showProgress(float time);
    }
}
