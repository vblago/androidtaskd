package ltd.vblago.taskd.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;
import ltd.vblago.taskd.R;
import ltd.vblago.taskd.fragment.StopwatchFragment;
import ltd.vblago.taskd.fragment.TabataTimerFragment;
import ltd.vblago.taskd.fragment.TimerFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, TimerFragment.newInstance())
                .commit();

    }

    @OnClick(R.id.stopwatch_btn)
    public void loadStopwatch(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, StopwatchFragment.newInstance())
                .commit();
    }

    @OnClick(R.id.timer_btn)
    public void loadTimer(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, TimerFragment.newInstance())
                .commit();
    }

    @OnClick(R.id.tabata_timer_btn)
    public void loadTabataTimer(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, TabataTimerFragment.newInstance())
                .commit();
    }
}
