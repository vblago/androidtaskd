package ltd.vblago.taskd.fragment;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.Unbinder;
import ltd.vblago.taskd.R;
import ltd.vblago.taskd.model.MyTabataTimer;

public class TabataTimerFragment extends Fragment {

    Unbinder unbinder;
    MyTabataTimer timer;

    @BindView(R.id.tabata_timer_view)
    TextView timerView;

    public static TabataTimerFragment newInstance() {
        return new TabataTimerFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tabata_timer, container, false);
        unbinder = ButterKnife.bind(this, root);

        timer = new MyTabataTimer();

        timer.setCallback(new MyTabataTimer.MyCallback() {
            @Override
            public void showProgress(float time) {
                timerView.setText(new DecimalFormat("#00.0").format(time));
            }

            @Override
            public void changeBackgroundColor(String color) {
                if (color.equals("work")) {
                    getView().setBackgroundColor(Color.RED);
                } else if (color.equals("relax")) {
                    getView().setBackgroundColor(Color.GREEN);
                } else {
                    getView().setBackgroundColor(Color.WHITE);
                }
            }
        });

        return root;
    }

    @OnClick(R.id.tabata_timer_view)
    public void timerClick() {
        if (timer.isStarted) {
            timer.stopTimer();
        } else {
            timer.startTimer();
        }
    }

    @OnLongClick(R.id.tabata_timer_view)
    public boolean timerLongClick() {
        timer.zeroingOut();
        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
