package ltd.vblago.taskd.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.Unbinder;
import ltd.vblago.taskd.R;
import ltd.vblago.taskd.model.MyStopwatch;

public class StopwatchFragment extends Fragment {

    Unbinder unbinder;
    MyStopwatch stopwatch;

    @BindView(R.id.stopwatch_view)
    TextView stopwatchView;

    public static StopwatchFragment newInstance() {
        return new StopwatchFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_stopwatch, container, false);
        unbinder = ButterKnife.bind(this, root);

        stopwatch = new MyStopwatch();

        stopwatch.setCallback(new MyStopwatch.MyCallback() {
            @Override
            public void showProgress(float[] time) {
                String str = String.format("%s.%s.%s",
                        new DecimalFormat("#00").format(time[2]),
                        new DecimalFormat("#00").format(time[1]),
                        new DecimalFormat("#00.0").format(time[0]));
                stopwatchView.setText(str);
            }
        });

        return root;
    }

    @OnClick(R.id.stopwatch_view)
    public void stopwatchClick() {
        if (stopwatch.isStarted) {
            stopwatch.stopStopwatch();
        } else {
            stopwatch.startStopwatch();
        }
    }

    @OnLongClick(R.id.stopwatch_view)
    public boolean stopwatchLongClick() {
        stopwatch.zeroingOut();
        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
