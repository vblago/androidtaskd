package ltd.vblago.taskd.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.Unbinder;
import ltd.vblago.taskd.R;
import ltd.vblago.taskd.model.MyTimer;

public class TimerFragment extends Fragment {

    Unbinder unbinder;
    MyTimer timer;

    @BindView(R.id.timer_view)
    TextView timerView;

    public static TimerFragment newInstance() {
        return new TimerFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_timer, container, false);
        unbinder = ButterKnife.bind(this, root);

        timer = new MyTimer();

        timer.setCallback(new MyTimer.MyCallback() {
            @Override
            public void showProgress(float time) {
                timerView.setText(new DecimalFormat("#00.0").format(time));
            }
        });

        return root;
    }

    @OnClick(R.id.timer_view)
    public void timerClick() {
        if (timer.isStarted) {
            timer.stopTimer();
        } else {
            timer.startTimer();
        }
    }

    @OnLongClick(R.id.timer_view)
    public boolean timerLongClick() {
        timer.zeroingOut();
        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
